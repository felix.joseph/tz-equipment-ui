/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular
        .module('equipment-category')
        .factory('EquipmentCategoryDataBuilder', EquipmentCategoryDataBuilder);

    EquipmentCategoryDataBuilder.$inject = ['EquipmentCategory'];

    function EquipmentCategoryDataBuilder(EquipmentCategory) {

        EquipmentCategoryDataBuilder.prototype.build = build;
        EquipmentCategoryDataBuilder.prototype.buildJson = buildJson;
        EquipmentCategoryDataBuilder.prototype.withId = withId;
        EquipmentCategoryDataBuilder.prototype.withName = withName;
        EquipmentCategoryDataBuilder.prototype.withCode = withCode;
        EquipmentCategoryDataBuilder.prototype.withDisplayOrder = withDisplayOrder;
        EquipmentCategoryDataBuilder.prototype.withoutId = withoutId;
        EquipmentCategoryDataBuilder.prototype.withActive = withActive;

        return EquipmentCategoryDataBuilder;

        function EquipmentCategoryDataBuilder() {
            this.id = '35b8eeca-bfad-47f3-b966-c9cb726b872f';
            this.name =  'Cold chain';
            this.code = 'cdch';
            this.displayOrder = 1;
            this.active = true;
        }

        function build() {
            return new EquipmentCategory(this.buildJson());
        }

        function withId(newId) {
            this.id = newId;
            return this;
        }

        function withName(name) {
            this.name = name;
            return this;
        }

        function withCode(code) {
            this.code = code;
            return this;
        }

        function withDisplayOrder(order) {
            this.displayOrder = order;
            return this;
        }

        function withoutId() {
            this.id = undefined;
            return this;
        }

        function withActive(active) {
            this.active = active;
            return this;
        }

        function buildJson() {
            return {
                id: this.id,
                name: this.name,
                code: this.code,
                displayOrder: this.displayOrder,
                active: this.active
            };
        }
    }

})();
