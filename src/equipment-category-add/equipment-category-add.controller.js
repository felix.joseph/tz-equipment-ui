/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipment-category-add.controller:EquipmentCategoryAddController
     *
     * @description
     * Controller for adding equipment category.
     */
    angular
        .module('equipment-category-add')
        .controller('EquipmentCategoryAddController', EquipmentCategoryAddController);

    EquipmentCategoryAddController.$inject = [
        'equipmentCategory', 'confirmService', 'stateTrackerService', '$state', 'equipmentCategoryService',
        'loadingModalService', 'equipmentTypes', 'disciplines', 'notificationService'
    ];

    function EquipmentCategoryAddController(equipmentCategory, confirmService, stateTrackerService, $state,
                                            equipmentCategoryService, loadingModalService, equipmentTypes,
                                            disciplines, notificationService) {

        var vm = this;
        vm.$onInit = onInit;
        vm.save = save;
        vm.goToPreviousState = stateTrackerService.goToPreviousState;

        function onInit() {
            vm.equipmentTypes = equipmentTypes;
            vm.disciplines = disciplines;
        }

        function save() {

            loadingModalService.open();
            return new equipmentCategoryService.save(vm.equipmentCategory)
                .then(function(equipmentCategory) {
                    notificationService.success('equipmentCategory.categoryHasBeenSaved');
                    stateTrackerService.goToPreviousState();
                    return equipmentCategory;
                })
                .catch(function() {
                    notificationService.error('equipmentCategory.failedToSaveCategory');
                    loadingModalService.close();
                });

        }
    }

})();
