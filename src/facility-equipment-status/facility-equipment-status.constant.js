/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc object
     * @name facility-equipment-status.FACILITY_EQUIPMENT_STATUS
     *
     * @description
     * Contains all possible equipment statuses.
     */
    angular
        .module('facility-equipment-status')
        .constant('FACILITY_EQUIPMENT_STATUS', statuses());

    function statuses() {
        var statuses = {
                getLabel: getLabel,
                getClass: getClass,
                ALL_FUNCTIONING: 'ALL_FUNCTIONING',
                NOT_FULLY_FUNCTIONING: 'NOT_FULLY_FUNCTIONING',
                NOT_FUNCTIONING: 'NOT_FUNCTIONING',
                UNKNOWN: 'UNKNOWN',
                LOADING: 'LOADING',
                NO_EQUIPMENT: 'NO_EQUIPMENT'
            },
            labels = {
                ALL_FUNCTIONING: 'facilityEquipmentStatus.allFunctioning',
                NOT_FULLY_FUNCTIONING: 'facilityEquipmentStatus.notFullyFunctioning',
                NOT_FUNCTIONING: 'facilityEquipmentStatus.notFunctioning',
                UNKNOWN: 'facilityEquipmentStatus.unknown',
                LOADING: 'facilityEquipmentStatus.loading',
                NO_EQUIPMENT: 'facilityEquipmentStatus.noEquipment'
            };

        return statuses;

        /**
         * @ngdoc method
         * @methodOf facility-equipment-status.FACILITY_EQUIPMENT_STATUS
         * @name getLabel
         *
         * @description
         * Returns the label for the given status.
         *
         * @param   {String}    status  the status to get the label for
         * @return  {String}            the label for the given status
         */
        function getLabel(status) {
            var label = labels[status];

            if (!label) {
                throw 'Invalid status';
            }

            return label;
        }

        /**
         * @ngdoc method
         * @methodOf facility-equipment-status.FACILITY_EQUIPMENT_STATUS
         * @name getClass
         *
         * @description
         * Returns a CSS class name defining the state of the status.
         *
         * @param   {String}    status  the status to get the class for
         * @return  {String}            the CSS class defining the state of the status
         */
        function getClass(status) {
            var statusClass;

            switch (status) {
            case statuses.ALL_FUNCTIONING:
                statusClass = 'is-functioning';
                break;
            case statuses.NOT_FULLY_FUNCTIONING:
                statusClass = 'is-not-fully-functioning';
                break;
            case statuses.NOT_FUNCTIONING:
                statusClass = 'is-non-functioning';
                break;
            case statuses.UNKNOWN:
                statusClass = 'is-unknown';
                break;
            case statuses.LOADING:
                statusClass = 'is-loading-status';
                break;
            case statuses.NO_EQUIPMENT:
                statusClass = 'no-equipment';
                break;
            default:
                throw 'Invalid status';
            }

            return statusClass;
        }
    }

})();
