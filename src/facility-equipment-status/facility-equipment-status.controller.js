/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name facility-equipment-status.controller:EquipmentStatusController
     *
     * @description
     * Exposes equipment status to the view.
     */
    angular
        .module('facility-equipment-status')
        .controller('EquipmentStatusController', EquipmentStatusController);

    EquipmentStatusController.$inject = ['FUNCTIONAL_STATUS', 'inventoryItemService',
        'FACILITY_EQUIPMENT_STATUS', 'permissionService', 'authorizationService', 'CCE_RIGHTS'];

    function EquipmentStatusController(FUNCTIONAL_STATUS, inventoryItemService, FACILITY_EQUIPMENT_STATUS,
                                       permissionService, authorizationService, CCE_RIGHTS) {
        var vm = this;

        vm.$onInit = onInit;
        vm.getFunctionalStatusClass = FUNCTIONAL_STATUS.getClass;

        /**
         * @ngdoc property
         * @propertyOf facility-equipment-status.controller:EquipmentStatusController
         * @name statusLabel
         * @type {String}
         *
         * @description
         * Holds status label.
         */
        vm.statusLabel = undefined;

        /**
         * @ngdoc property
         * @propertyOf facility-equipment-status.controller:EquipmentStatusController
         * @name statusClass
         * @type {String}
         *
         * @description
         * Holds status class.
         */
        vm.statusClass = undefined;

        /**
         * @ngdoc property
         * @propertyOf facility-equipment-status.controller:EquipmentStatusController
         * @name inventoryItems
         * @type {Array}
         *
         * @description
         * Holds list of inventory items for facility.
         */
        vm.inventoryItems = undefined;

        /**
         * @ngdoc property
         * @propertyOf facility-equipment-status.controller:EquipmentStatusController
         * @name equipmentAvailable
         * @type {boolean}
         *
         * @description
         * Indicator if there is equipment available.
         */
        vm.equipmentAvailable = undefined;

        /**
         * @ngdoc method
         * @methodOf facility-equipment-status.controller:EquipmentStatusController
         * @name $onInit
         *
         * @description
         * Initialization method of the EquipmentStatusController.
         */
        function onInit() {
            setLabelAndClass(FACILITY_EQUIPMENT_STATUS.LOADING);

            var authUser = authorizationService.getUser();
            var permission = {
                facilityId: vm.facility.id,
                right: CCE_RIGHTS.CCE_INVENTORY_VIEW
            };
            permissionService.hasPermissionWithAnyProgram(authUser.user_id, permission)
                .then(function() {
                    return inventoryItemService.getAllForFacility(vm.facility.id);
                })
                .then(function(list) {
                    vm.inventoryItems = list;
                    if (vm.inventoryItems.length === 0) {
                        vm.equipmentAvailable = false;
                    } else {
                        vm.equipmentAvailable = true;
                    }
                    var status = getStatus(list);
                    setLabelAndClass(status);
                })
                .catch(function() {
                    setLabelAndClass(FACILITY_EQUIPMENT_STATUS.UNKNOWN);
                });
        }

        /**
         * @ngdoc method
         * @methodOf facility-equipment-status.controller:EquipmentStatusController
         * @name getStatus
         *
         * @description
         * Returns a Status based on inventory response.
         *
         * @param   {Object}    list    equipment status
         * @return  {String}            the label for the inventory item
         */
        function getStatus(list) {
            if (!vm.equipmentAvailable) {
                return FACILITY_EQUIPMENT_STATUS.NO_EQUIPMENT;
            }
            var notFunctioningInventoryItems = filterNotFunctioningInventoryItems(list);
            if (notFunctioningInventoryItems.length === list.length || list.length === 0) {
                return FACILITY_EQUIPMENT_STATUS.NOT_FUNCTIONING;
            } else if (notFunctioningInventoryItems.length > 0) {
                return FACILITY_EQUIPMENT_STATUS.NOT_FULLY_FUNCTIONING;
            }
            return FACILITY_EQUIPMENT_STATUS.ALL_FUNCTIONING;
        }

        function filterNotFunctioningInventoryItems(list) {
            return list.filter(function(inventory) {
                return inventory.functionalStatus !== FUNCTIONAL_STATUS.FUNCTIONING;
            });
        }

        function setLabelAndClass(status) {
            vm.statusLabel = FACILITY_EQUIPMENT_STATUS.getLabel(status);
            vm.statusClass = FACILITY_EQUIPMENT_STATUS.getClass(status);
        }
    }

})();
