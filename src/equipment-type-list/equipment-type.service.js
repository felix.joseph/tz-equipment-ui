/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name equipmentTypeService
     *
     * @description
     * Responsible for retrieving equipment types from the server.
     */
    angular
        .module('equipment-type-list')
        .service('equipmentTypeService', service);

    service.$inject = ['equipmentUrlFactory', '$resource'];

    function service(equipmentUrlFactory, $resource) {

        var resource = $resource(equipmentUrlFactory('/api/equipmentTypes/:id'), {}, {
            query: {
                url: equipmentUrlFactory('/api/equipmentTypes'),
                method: 'GET',
                isArray: false
            },
            getAll: {
                url: equipmentUrlFactory('/api/equipmentTypes'),
                method: 'GET'
            },
            get: {
                url: equipmentUrlFactory('/api/equipmentTypes/:id'),
                method: 'GET'
            },
            update: {
                method: 'PUT'
            }
        });
        this.getAll = getAll;
        this.query = query;
        this.get = get;
        this.save = save;

        function getAll(paginationParams) {
            return resource.getAll(paginationParams).$promise;
        }

        /**
             * @ngdoc method
             * @methodOf equipmentTypeService
             * @name getAll
             *
             * @description
             * Retrieves all equipment types by ids.
             *
             * @param  {Object}  queryParams the search parameters
             * @return {Promise} Page of equipment types
             */
        function query(queryParams) {
            return resource.query(queryParams).$promise;
        }

        function get(id) {
            return resource.get({
                id: id
            }).$promise;
        }

        function save(type) {
            if (type.id) {
                return resource.update({
                    id: type.id
                }, type).$promise;
            }
            return resource.save({}, type).$promise;
        }

    }
})();
