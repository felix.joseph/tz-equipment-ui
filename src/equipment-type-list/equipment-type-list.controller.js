/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipment-type.controller:EquipmentTypeListController
     *
     * @description
     * Controller for lab equipment type list.
     */
    angular
        .module('equipment-type-list')
        .controller('EquipmentTypeListController', EquipmentTypeListController);

    EquipmentTypeListController.$inject = [
        '$state', '$stateParams', 'CCE_RIGHTS', 'messageService', 'equipmentTypes'
    ];

    function EquipmentTypeListController($state, $stateParams, CCE_RIGHTS, messageService, equipmentTypes) {
        var vm = this;
        vm.$onInit = onInit;

        vm.goToAddEquipmentTypePage = goToAddEquipmentTypePage;

        /**
         * @ngdoc property
         * @propertyOf equipment-type.controller:EquipmentTypeListController
         * @name equipmentTypes
         * @type {Array}
         *
         * @description
         * Contains page of Equipment Types.
         */
        vm.equipmentTypes = undefined;
        /**
         * @ngdoc method
         * @methodOf equipment-type.controller:EquipmentTypeListController
         * @name onInit
         *
         * @description
         * Init method for EquipmentTypeListController.
         */
        function onInit() {
            vm.equipmentTypes = equipmentTypes;
        }

        function goToAddEquipmentTypePage() {
            $state.go('openlmis.administration.equipmentTypes.add');
        }

    }

})();
