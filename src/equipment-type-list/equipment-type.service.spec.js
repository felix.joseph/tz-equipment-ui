/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('equipmentTypeService', function() {

    var $rootScope, $httpBackend;
    var equipmentUrlFactory, equipmentTypeService, EquipmentTypeDataBuilder;
    var equipmentType, equipmentTypes, EQUIPMENT_TYPE_ID;

    beforeEach(function() {
        module('equipment-type-list');

        inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $httpBackend = $injector.get('$httpBackend');
            equipmentUrlFactory = $injector.get('equipmentUrlFactory');
            equipmentTypeService = $injector.get('equipmentTypeService');
            EquipmentTypeDataBuilder = $injector.get('EquipmentTypeDataBuilder');
        });

        prepareTestData();
    });

    describe('get', function() {

        beforeEach(function() {
            $httpBackend.when('GET', equipmentUrlFactory('/api/equipmentTypes/' + equipmentTypes[0].id))
                .respond(200, equipmentTypes[0]);
        });

        it('should return promise', function() {
            var result = equipmentTypeService.get(equipmentTypes[0].id);
            $httpBackend.flush();

            expect(result.then).not.toBeUndefined();
        });

        it('should resolve to equipmentType', function() {
            var result;

            equipmentTypeService.get(equipmentTypes[0].id).then(function(data) {
                result = data;
            });
            $httpBackend.flush();
            $rootScope.$apply();

            var expected = angular.copy(equipmentTypes[0]);

            expect(angular.toJson(result)).toEqual(angular.toJson(expected));
        });

        it('should make a proper request', function() {
            $httpBackend.expectGET(equipmentUrlFactory('/api/equipmentTypes/' + equipmentTypes[0].id));

            equipmentTypeService.get(equipmentTypes[0].id);
            $httpBackend.flush();
        });
    });

    describe('getAll', function() {

        var parameters = {
            page: 0,
            size: 10
        };

        beforeEach(function() {
            $httpBackend.when('GET', equipmentUrlFactory('/api/equipmentTypes?page=' + parameters.page +
                '&size=' + parameters.size)).respond(200, {
                content: equipmentTypes
            });
        });

        it('should return promise', function() {
            var result = equipmentTypeService.getAll(parameters);
            $httpBackend.flush();

            expect(result.then).not.toBeUndefined();
        });

        it('should resolve to equipmentTypes', function() {
            var result;

            equipmentTypeService.getAll(parameters).then(function(data) {
                result = data;
            });
            $httpBackend.flush();
            $rootScope.$apply();

            var expected = angular.copy(equipmentTypes);

            expect(angular.toJson(result)).toEqual(angular.toJson({
                content: expected
            }));
        });

        it('should make a proper request', function() {
            $httpBackend.expectGET(equipmentUrlFactory('/api/equipmentTypes?page=' + parameters.page +
                '&size=' + parameters.size));

            equipmentTypeService.getAll(parameters);
            $httpBackend.flush();
        });
    });

    describe('save', function() {

        var result;

        beforeEach(function() {
            result = undefined;
        });

        it('should create equipmentType if ID is not given', function() {
            equipmentType = new EquipmentTypeDataBuilder().withoutId()
                .build();

            var returned = angular.copy(equipmentType);

            returned.id = EQUIPMENT_TYPE_ID;

            $httpBackend.expect(
                'POST', equipmentUrlFactory('/api/equipmentTypes'), equipmentType
            ).respond(200, returned);

            equipmentTypeService.save(equipmentType).then(function(equipmentType) {
                result = equipmentType;
            });

            $httpBackend.flush();
            $rootScope.$apply();

            expect(angular.toJson(result)).toEqual(angular.toJson(returned));
        });

        it('should update equipmentType if it has ID', function() {
            equipmentType = new EquipmentTypeDataBuilder().withId(EQUIPMENT_TYPE_ID)
                .build();

            $httpBackend.expect(
                'PUT', equipmentUrlFactory('/api/equipmentTypes/' + EQUIPMENT_TYPE_ID), equipmentType
            ).respond(200, equipmentType);

            equipmentTypeService.save(equipmentType).then(function(equipmentType) {
                result = equipmentType;
            });

            $httpBackend.flush();
            $rootScope.$apply();

            expect(angular.toJson(result)).toEqual(angular.toJson(equipmentType));
        });

    });

    afterEach(function() {
        $httpBackend.verifyNoOutstandingRequest();
        $httpBackend.verifyNoOutstandingExpectation();
    });

    function prepareTestData() {
        EQUIPMENT_TYPE_ID = 'some-equipment-type-id';

        equipmentTypes = [
            new EquipmentTypeDataBuilder().withId('35b8eeca-bfad-47f3-b966-c9cb726b872f')
                .build()

        ];
    }
});
