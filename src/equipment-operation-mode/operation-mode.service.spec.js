/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('operationModeService', function() {

    var $rootScope, $httpBackend;
    var equipmentUrlFactory, operationModeService, OperationModeDataBuilder;
    var operationModes, operationMode, OPERATION_MODE_ID;

    beforeEach(function() {
        module('equipment-operation-mode');

        inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $httpBackend = $injector.get('$httpBackend');
            equipmentUrlFactory = $injector.get('equipmentUrlFactory');
            operationModeService = $injector.get('operationModeService');
            OperationModeDataBuilder = $injector.get('OperationModeDataBuilder');
        });

        prepareTestData();
    });

    describe('get', function() {

        beforeEach(function() {
            $httpBackend.when('GET', equipmentUrlFactory('/api/equipmentOperationModes/' + operationModes[0].id))
                .respond(200, operationModes[0]);
        });

        it('should return promise', function() {
            var result = operationModeService.get(operationModes[0].id);
            $httpBackend.flush();

            expect(result.then).not.toBeUndefined();
        });

        it('should resolve to operationMode', function() {
            var result;

            operationModeService.get(operationModes[0].id).then(function(data) {
                result = data;
            });
            $httpBackend.flush();
            $rootScope.$apply();

            var expected = angular.copy(operationModes[0]);

            expect(angular.toJson(result)).toEqual(angular.toJson(expected));
        });

        it('should make a proper request', function() {
            $httpBackend.expectGET(equipmentUrlFactory('/api/equipmentOperationModes/' + operationModes[0].id));

            operationModeService.get(operationModes[0].id);
            $httpBackend.flush();
        });
    });

    describe('getAll', function() {

        var parameters = {
            page: 0,
            size: 10
        };

        beforeEach(function() {
            $httpBackend.when('GET', equipmentUrlFactory('/api/equipmentOperationModes?page=' + parameters.page +
                '&size=' + parameters.size)).respond(200, {
                content: operationModes
            });
        });

        it('should return promise', function() {
            var result = operationModeService.getAll(parameters);
            $httpBackend.flush();

            expect(result.then).not.toBeUndefined();
        });

        it('should resolve to operationModes', function() {
            var result;

            operationModeService.getAll(parameters).then(function(data) {
                result = data;
            });
            $httpBackend.flush();
            $rootScope.$apply();

            var expected = angular.copy(operationModes);

            expect(angular.toJson(result)).toEqual(angular.toJson({
                content: expected
            }));
        });

        it('should make a proper request', function() {
            $httpBackend.expectGET(equipmentUrlFactory('/api/equipmentOperationModes?page=' + parameters.page +
                '&size=' + parameters.size));

            operationModeService.getAll(parameters);
            $httpBackend.flush();
        });
    });

    describe('save', function() {

        var result;

        beforeEach(function() {
            result = undefined;
        });

        it('should create operationMode if ID is not given', function() {
            operationMode = new OperationModeDataBuilder().withoutId()
                .build();

            var returned = angular.copy(operationMode);

            returned.id = OPERATION_MODE_ID;

            $httpBackend.expect(
                'POST', equipmentUrlFactory('/api/equipmentOperationModes'), operationMode
            ).respond(200, returned);

            operationModeService.save(operationMode).then(function(operationMode) {
                result = operationMode;
            });

            $httpBackend.flush();
            $rootScope.$apply();

            expect(angular.toJson(result)).toEqual(angular.toJson(returned));
        });

        it('should update operationMode if it has ID', function() {
            operationMode = new OperationModeDataBuilder().withId(OPERATION_MODE_ID)
                .build();

            $httpBackend.expect(
                'PUT', equipmentUrlFactory('/api/equipmentOperationModes/' + OPERATION_MODE_ID), operationMode
            ).respond(200, operationMode);

            operationModeService.save(operationMode).then(function(operationMode) {
                result = operationMode;
            });

            $httpBackend.flush();
            $rootScope.$apply();

            expect(angular.toJson(result)).toEqual(angular.toJson(operationMode));
        });

    });

    afterEach(function() {
        $httpBackend.verifyNoOutstandingRequest();
        $httpBackend.verifyNoOutstandingExpectation();
    });

    function prepareTestData() {
        OPERATION_MODE_ID = 'some-operation-mode-id';

        operationModes = [
            new OperationModeDataBuilder().withId('1')
                .build(),
            new OperationModeDataBuilder().withId('2')
                .build()
        ];
    }
});
