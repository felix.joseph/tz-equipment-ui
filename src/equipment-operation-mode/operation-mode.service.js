/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name equipment-operation-mode.operationModeService
     *
     * @description
     * Responsible for retrieving operation modes from the server.
     */
    angular
        .module('equipment-operation-mode')
        .factory('operationModeService', service);

    service.$inject = ['openlmisUrlFactory', '$resource'];

    function service(openlmisUrlFactory, $resource) {

        var resource = $resource(openlmisUrlFactory('/api/equipmentOperationModes/:id'), {}, {
            getAll: {
                url: openlmisUrlFactory('/api/equipmentOperationModes'),
                method: 'GET'
            },
            update: {
                method: 'PUT'
            }
        });

        return {
            get: get,
            getAll: getAll,
            save: save
        };

        /**
         * @ngdoc method
         * @methodOf equipment-operation-mode.operationModeService
         * @name get
         *
         * @description
         * Gets operationMode by id.
         *
         * @param  {String}  id OperationMode UUID
         * @return {Promise}    OperationMode info
         */
        function get(id) {
            return resource.get({
                id: id
            }).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-operation-mode.operationModeService
         * @name getAll
         *
         * @description
         * Gets all operationModes.
         *
         * @return {Promise} Array of all operationModes
         */
        function getAll(paginationParams) {
            return resource.getAll(paginationParams).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-operation-mode.operationModeService
         * @name create
         *
         * @description
         * Saves the given operation mode. It will create a new one if the ID is not defined and
         * update the existing one otherwise.
         *
         * @param  {Object}  operationMode OperationMode to be created/updated
         * @return {Promise}         the promise resolving to the created/updated operationMode
         */
        function save(operationMode) {
            if (operationMode.id) {
                return resource.update({
                    id: operationMode.id
                }, operationMode).$promise;
            }
            return resource.save({}, operationMode).$promise;
        }
    }
})();

