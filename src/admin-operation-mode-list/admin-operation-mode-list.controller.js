/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name admin-operation-mode-list.controller:OperationModeListController
     *
     * @description
     * Controller for equipment operation-mode list screen.
     */
    angular
        .module('admin-operation-mode-list')
        .controller('OperationModeListController', OperationModeListController);

    OperationModeListController.$inject = [
        '$state', '$stateParams', 'CCE_RIGHTS', 'messageService', 'operationModes'
    ];

    function OperationModeListController($state, $stateParams, CCE_RIGHTS, messageService, operationModes) {

        var vm = this;
        vm.$onInit = onInit;
        vm.goToAddOperationModePage = goToAddOperationModePage;

        /**
         * @ngdoc property
         * @propertyOf admin-operation-mode-list.controller:OperationModeListController
         * @name operationModes
         * @type {Array}
         *
         * @description
         * Init method for OperationModeListController.
         */
        vm.operationModes = undefined;
        /**
         * @ngdoc method
         * @methodOf admin-operation-mode-list.controller:OperationModeListController
         * @name onInit
         *
         * @description
         * Init method for OperationModeListController.
         */
        function onInit() {
            vm.operationModes = operationModes;
        }

        function goToAddOperationModePage() {
            $state.go('openlmis.administration.operationModes.add');
        }

    }

})();
