/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name discipline-add.controller:DisciplineAddController
     *
     * @description
     * Provides methods for Add Discpline modal. Allows returning to previous states and saving
     * discipline.
     */
    angular
        .module('discipline-add')
        .controller('DisciplineAddController', DisciplineAddController);

    DisciplineAddController.$inject = [
        'discipline', 'confirmService', 'stateTrackerService', '$state', 'loadingModalService',
        'notificationService', 'messageService', 'disciplineService'
    ];

    function DisciplineAddController(discipline, confirmService, stateTrackerService,
                                     $state, loadingModalService, notificationService,
                                     messageService, disciplineService) {
        var vm = this;

        vm.$onInit = onInit;
        vm.save = save;
        vm.goToPreviousState = stateTrackerService.goToPreviousState;

        /**
         * @ngdoc method
         * @methodOf discipline-add.controller:DisciplineAddController
         * @name $onInit
         *
         * @description
         * Initialization method of the DisciplineAddController.
         */
        function onInit() {
            vm.discipline = discipline;
            vm.discipline.enabled = discipline.enabled !== false;
        }

        function save() {
            loadingModalService.open();
            return new disciplineService.save(vm.discipline)
                .then(function(discipline) {
                    notificationService.success('disciplineAdd.disciplineHasBeenSaved');
                    stateTrackerService.goToPreviousState();
                    return discipline;
                })
                .catch(function() {
                    notificationService.error('disciplineAdd.failedToSaveDiscipline');
                    loadingModalService.close();
                });
        }
    }

})();
