/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipment-inventory-list.controller:EquipmentInventoryListController
     *
     * @description
     * Controller for equipment inventory item list screen.
     */
    angular
        .module('equipment-inventory-list')
        .controller('EquipmentInventoryListController', EquipmentInventoryListController);

    EquipmentInventoryListController.$inject = [
        'inventoryItems', '$state', '$stateParams', 'FUNCTIONAL_STATUS', 'CCE_RIGHTS', 'messageService',
        'REASON_FOR_NOT_WORKING', 'canEdit'
    ];

    function EquipmentInventoryListController(inventoryItems, $state, $stateParams, FUNCTIONAL_STATUS,
                                              CCE_RIGHTS, messageService, REASON_FOR_NOT_WORKING, canEdit) {

        var vm = this;

        vm.$onInit = onInit;
        vm.getStatusLabel = getStatusLabel;
        vm.goToStatusUpdate = goToStatusUpdate;
        vm.getReasonLabel = getReasonLabel;
        vm.search = search;
        vm.getFunctionalStatusClass = FUNCTIONAL_STATUS.getClass;

        /**
         * @ngdoc property
         * @propertyOf equipment-inventory-list.controller:EquipmentInventoryListController
         * @name inventoryItems
         * @type {Array}
         *
         * @description
         * Init method for EquipmentInventoryListController.
         */
        vm.inventoryItems = undefined;

        /**
         * @ngdoc property
         * @propertyOf equipment-inventory-list.controller:EquipmentInventoryListController
         * @name userHasRightToEdit
         * @type {Boolean}
         *
         * @description
         * Flag defining whether user has right for editing the list of inventory items.
         */
        vm.userHasRightToEdit = undefined;

        /**
         * @ngdoc property
         * @propertyOf equipment-inventory-list.controller:EquipmentInventoryListController
         * @name facility
         * @type {string}
         *
         * @description
         * The facility that inventory items will be filtered by.
         */

        /**
         * @ngdoc property
         * @propertyOf equipment-inventory-list.controller:EquipmentInventoryListController
         * @name program
         * @type {string}
         *
         * @description
         * The program that inventory items will be filtered by.
         */

        /**
         * @ngdoc property
         * @propertyOf equipment-inventory-list.controller:EquipmentInventoryListController
         * @name isSupervised
         * @type {Boolean}
         *
         * @description
         * Holds currently selected facility selection type:
         *  false - my facility
         *  true - supervised facility
         */
        vm.isSupervised = undefined;

        /**
         * ngdoc property
         * @propertyOf equipment-inventory-list.controller.EquipmentInventoryListController
         * @name functionalStatus
         * @type {string}
         *
         * @description
         * The selected functional status.
         */
        vm.functionalStatus = undefined;

        /**
         * ngdoc property
         * @propertyOf equipment-inventory-list.controller.EquipmentInventoryListController
         * @name functionalStatuses
         * @type {Array}
         *
         * @description
         * The list of available functional statuses.
         */
        vm.functionalStatuses = undefined;

        /**
         * @ngdoc method
         * @methodOf equipment-inventory-list.controller:EquipmentInventoryListController
         * @name onInit
         *
         * @description
         * Init method for EquipmentInventoryListController.
         */
        function onInit() {
            vm.inventoryItems = inventoryItems;
            vm.functionalStatuses = FUNCTIONAL_STATUS.getStatuses();
            vm.functionalStatus = $stateParams.functionalStatus;
            vm.userHasRightToEdit = canEdit;
            vm.isSupervised = $stateParams.supervised;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-inventory-list.controller:EquipmentInventoryListController
         * @name getStatusLabel
         *
         * @description
         * Return localized label for the functional status.
         *
         * @param   {string}    status  the status to get the label for
         * @return  {string}            the localized status label
         */
        function getStatusLabel(status) {
            return messageService.get(FUNCTIONAL_STATUS.getLabel(status));
        }

        /**
         * @ngdoc method
         * @methodOf equipment-inventory-list.controller:EquipmentInventoryListController
         * @name getReasonLabel
         *
         * @description
         * Return localized label for the reason.
         *
         * @param   {string}    reason  the reason to get the label for
         * @return  {string}            the localized reason label
         */
        function getReasonLabel(reason) {
            if (!reason) {
                return '';
            }
            return messageService.get(REASON_FOR_NOT_WORKING.getLabel(reason));
        }

        /**
         * @ngdoc method
         * @methodOf equipment-inventory-list.controller:EquipmentInventoryListController
         * @name goToStatusUpdate
         *
         * @description
         * Redirects user to inventory item status update modal screen.
         *
         * @param {InventoryItem} inventoryItem the inventory item which status will be updated
         */
        function goToStatusUpdate(inventoryItem) {
            $state.go('openlmis.equipment.inventory.item.statusUpdate', {
                inventoryItem: inventoryItem,
                inventoryItemId: inventoryItem.id
            });
        }

        /**
         * @ngdoc method
         * @methodOf equipment-inventory-list.controller:EquipmentInventoryListController
         * @name search
         *
         * @description
         * Reloads page with the new search parameters.
         */
        function search() {
            var stateParams = angular.copy($stateParams);

            delete stateParams.facilityId;
            delete stateParams.programId;

            stateParams.facility = vm.facility.id;
            stateParams.program = vm.program.id;
            stateParams.functionalStatus = vm.functionalStatus;
            stateParams.supervised = vm.isSupervised;

            $state.go('openlmis.equipment.inventory', stateParams, {
                reload: true
            });
        }
    }

})();
