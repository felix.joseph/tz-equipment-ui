/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipment-add-inventory-item.controller:AddInventoryItemController
     *
     * @description
     * Manages Add Inventory Item modal.
     */
    angular
        .module('equipment-add-inventory-item')
        .controller('AddInventoryItemController', controller);

    controller.$inject = ['$state', 'types', 'catalogItems', '$scope', 'confirmService'];

    function controller($state, types, catalogItems, $scope, confirmService) {
        var vm = this;

        vm.$onInit = onInit;
        vm.addInventoryItem = addInventoryItem;
        vm.clearMakeModelSelection = clearMakeModelSelection;
        vm.goToInventoryList = goToInventoryList;

        /**
         * @ngdoc property
         * @propertyOf equipment-add-inventory-item.controller:AddInventoryItemController
         * @type {Array}
         * @name types
         *
         * @description
         * The list of available equipment types.
         */
        vm.types = undefined;

        /**
         * @ngdoc property
         * @propertyOf equipment-add-inventory-item.controller:AddInventoryItemController
         * @type {Array}
         * @name catalogItems
         *
         * @description
         * The list of available catalog items;
         */
        vm.catalogItems = undefined;

        /**
         * @ngdoc method
         * @methodOf equipment-add-inventory-item.controller:AddInventoryItemController
         * @name $onInit
         *
         * @description
         * Initialization method of the AddInventoryItemController.
         */
        function onInit() {
            vm.types = types;
            vm.catalogItems = catalogItems;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-add-inventory-item.controller:AddInventoryItemController
         * @name addInventoryItem
         *
         * @description
         * Redirects the user to the edit modal.
         */
        function addInventoryItem() {
            $state.go('openlmis.equipment.inventory.edit', {
                inventoryItem: {
                    facility: vm.facility,
                    programId: vm.program.id,
                    program: vm.program,
                    catalogItem: vm.catalogItem
                }
            });
        }

        /**
         * @ngdoc method
         * @methodOf equipment-add-inventory-item.controller:AddInventoryItemController
         * @name clearMakeModelSelection
         *
         * @description
         * Clears the selected catalog item.
         */
        function clearMakeModelSelection() {
            vm.catalogItem = undefined;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-add-inventory-item.controller:AddInventoryItemController
         * @name goToInventoryList
         *
         * @description
         * Takes the user to the inventory item list screen. Will open a confirmation modal if user
         * interacted with the form.
         */
        function goToInventoryList() {
            if ($scope.addInventoryItemForm.$dirty) {
                confirmService.confirm(
                    'equipmentAddInventoryItem.closeAddInventoryItemModal',
                    'equipmentAddInventoryItem.yes',
                    'equipmentAddInventoryItem.no'
                ).then(function() {
                    $state.go('openlmis.equipment.inventory');
                });
            } else {
                $state.go('openlmis.equipment.inventory');
            }
        }

    }

})();
