/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name discipline-list.controller:DisciplineListController
     *
     * @description
     * Controller for lab discipline list.
     */
    angular
        .module('discipline-list')
        .controller('DisciplineListController', DisciplineListController);

    DisciplineListController.$inject = [
        '$state', '$stateParams', 'CCE_RIGHTS', 'disciplines'
    ];

    function DisciplineListController($state, $stateParams, CCE_RIGHTS, disciplines) {

        var vm = this;
        vm.$onInit = onInit;
        vm.goToAddDisciplinePage = goToAddDisciplinePage;

        /**
         * @ngdoc property
         * @propertyOf discipline-list.controller:DisciplineListController
         * @name disciplines
         * @type {Array}
         *
         * @description
         * Init method for DisciplineListController.
         */
        vm.disciplines = undefined;
        /**
         * @ngdoc method
         * @methodOf discipline-list.controller:DisciplineListController
         * @name onInit
         *
         * @description
         * Init method for DisciplineListController.
         */
        function onInit() {
            vm.disciplines = disciplines;
        }

        function goToAddDisciplinePage() {
            $state.go('openlmis.administration.discipline.add');
        }

    }

})();
