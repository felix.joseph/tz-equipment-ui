/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('openlmis.administration.discipline state', function() {

    'use strict';

    var DisciplineDataBuilder, $state, $rootScope, disciplineService, $q, $templateCache,
        disciplines, goToUrl, getResolvedValue, $location, PageDataBuilder;
    beforeEach(function() {
        module('discipline-list');

        inject(function($injector) {
            $state = $injector.get('$state');
            $location = $injector.get('$location');
            $rootScope = $injector.get('$rootScope');
            disciplineService = $injector.get('disciplineService');
            $q = $injector.get('$q');
            $templateCache = $injector.get('$templateCache');
            DisciplineDataBuilder = $injector.get('DisciplineDataBuilder');
            PageDataBuilder = $injector.get('PageDataBuilder');
        });

        disciplines = [
            new DisciplineDataBuilder().build(),
            new DisciplineDataBuilder().build()
        ];

        spyOn(disciplineService, 'getAll').andReturn($q.when(
            new PageDataBuilder()
                .withContent(disciplines)
                .build()
        ));

        $state.go('openlmis');
        $rootScope.$apply();

        goToUrl = function(url) {
            $location.url(url);
            $rootScope.$apply();
        };

        getResolvedValue = function(name) {
            return $state.$current.locals.globals[name];
        };
    });

    it('should be available under /administration/disciplines URL', function() {
        expect($state.current.name).not.toEqual('openlmis.administration.discipline');

        goToUrl('/administration/disciplines');

        expect($state.current.name).toEqual('openlmis.administration.discipline');
    });

    it('should resolve disciplines', function() {
        disciplineService.getAll.andReturn($q.when({
            content: disciplines
        }));

        goToUrl('/administration/disciplines');

        expect($state.current.name).toEqual('openlmis.administration.discipline');
        expect(getResolvedValue('disciplines')).toEqual(disciplines);
    });

    it('should use template', function() {
        spyOn($templateCache, 'get').andCallThrough();

        goToUrl('/administration/disciplines');

        expect($templateCache.get).toHaveBeenCalledWith('discipline-list/discipline-list.html');
    });

});
