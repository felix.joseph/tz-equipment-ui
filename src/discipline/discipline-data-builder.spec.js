/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular
        .module('discipline')
        .factory('DisciplineDataBuilder', DisciplineDataBuilder);

    DisciplineDataBuilder.$inject = ['Discipline'];

    function DisciplineDataBuilder(Discipline) {

        DisciplineDataBuilder.prototype.build = build;
        DisciplineDataBuilder.prototype.buildJson = buildJson;
        DisciplineDataBuilder.prototype.withId = withId;
        DisciplineDataBuilder.prototype.withName = withName;
        DisciplineDataBuilder.prototype.withCode = withCode;
        DisciplineDataBuilder.prototype.withDisplayOrder = withDisplayOrder;
        DisciplineDataBuilder.prototype.withoutId = withoutId;
        DisciplineDataBuilder.prototype.withEnabled = withEnabled;

        return DisciplineDataBuilder;

        function DisciplineDataBuilder() {
            this.id = '35b8eeca-bfad-47f3-b966-c9cb726b872f';
            this.name =  'immunology';
            this.code = 'immu';
            this.displayOrder = 1;
            this.enabled = true;
        }

        function build() {
            return new Discipline(this.buildJson());
        }

        function withId(newId) {
            this.id = newId;
            return this;
        }

        function withName(name) {
            this.name = name;
            return this;
        }

        function withCode(code) {
            this.code = code;
            return this;
        }

        function withDisplayOrder(order) {
            this.displayOrder = order;
            return this;
        }

        function withoutId() {
            this.id = undefined;
            return this;
        }

        function withEnabled(enabled) {
            this.enabled = enabled;
            return this;
        }

        function buildJson() {
            return {
                id: this.id,
                name: this.name,
                code: this.code,
                displayOrder: this.displayOrder,
                enabled: this.enabled
            };
        }
    }

})();
