/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('OperationModeAddController', function() {

    beforeEach(function() {
        module('admin-operation-mode-add');

        inject(function($injector) {
            this.$controller = $injector.get('$controller');
            this.$rootScope = $injector.get('$rootScope');
            this.$q = $injector.get('$q');
            this.operationModeService = $injector.get('operationModeService');
            this.stateTrackerService = $injector.get('stateTrackerService');
            this.loadingModalService = $injector.get('loadingModalService');
            this.notificationService = $injector.get('notificationService');
            this.OperationModeDataBuilder = $injector.get('OperationModeDataBuilder');
        });

        this.operationMode = new this.OperationModeDataBuilder().build();

        this.saveDeferred = this.$q.defer();

        spyOn(this.stateTrackerService, 'goToPreviousState').andReturn(true);
        spyOn(this.operationModeService, 'save').andReturn(this.saveDeferred.promise);
        spyOn(this.loadingModalService, 'open').andReturn(true);
        spyOn(this.loadingModalService, 'close').andReturn(true);
        spyOn(this.notificationService, 'success');
        spyOn(this.notificationService, 'error');

        this.vm = this.$controller('OperationModeAddController', {
            operationMode: this.operationMode
        });
        this.vm.$onInit();
    });

    describe('save', function() {

        it('should reload state after successful save', function() {
            this.vm.save();

            this.saveDeferred.resolve(this.operationMode);
            this.$rootScope.$apply();

            expect(this.stateTrackerService.goToPreviousState).toHaveBeenCalled();
        });

        describe('while creatin', function() {

            it('should show notification if operation mode was saveed successfully', function() {
                this.vm.save();

                this.saveDeferred.resolve(this.operationMode);
                this.$rootScope.$apply();

                expect(this.notificationService.success).toHaveBeenCalledWith(
                    'adminOperationModeAdd.operationModeHasBeenSaved'
                );
            });

            it('should show notification if operation mode save has failed', function() {
                this.vm.save();

                this.saveDeferred.reject();
                this.$rootScope.$apply();

                expect(this.notificationService.error).toHaveBeenCalledWith(
                    'adminOperationModeAdd.failedToSaveOperationMode'
                );

                expect(this.loadingModalService.close).toHaveBeenCalled();
            });
        });
    });
});