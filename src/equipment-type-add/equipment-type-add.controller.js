/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipment-type.controller:EquipmentTypeAddController
     *
     * @description
     * Provides methods for Add Equipment Type. Allows returning to previous states and saving
     * equipment type.
     */
    angular
        .module('equipment-type-add')
        .controller('EquipmentTypeAddController', EquipmentTypeAddController);

    EquipmentTypeAddController.$inject = [
        'equipmentType', '$state',  'stateTrackerService', 'loadingModalService',
        'equipmentTypeService', 'notificationService'
    ];

    function EquipmentTypeAddController(equipmentType, $state, stateTrackerService,
                                        loadingModalService, equipmentTypeService, notificationService) {

        var vm = this;

        vm.$onInit = onInit;
        vm.save = save;

        vm.goToPreviousState = stateTrackerService.goToPreviousState;

        /**
         * @ngdoc method
         * @methodOf equipment-type.controller:EquipmentTypeAddController
         * @name $onInit
         *
         * @description
         * Initialization method of the EquipmentTypeAddController.
         */
        function onInit() {
            vm.equipmentType = equipmentType;
            vm.equipmentType.isColdChain = false;
            vm.equipmentType.isBioChemistry = false;
            vm.equipmentType.active = true;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-type.controller:EquipmentTypeAddController
         * @name save
         *
         * @description
         * Saves the equipment type and takes user back to the previous state.
         */
        function save() {
            loadingModalService.open();
            return new equipmentTypeService.save(vm.equipmentType)
                .then(function(equipmentType) {
                    notificationService.success('equipmentType.typeHasBeenSaved');
                    stateTrackerService.goToPreviousState();
                    return equipmentType;
                })
                .catch(function() {
                    notificationService.error('equipmentType.failedToSaveType');
                    loadingModalService.close();
                });
        }

    }

})();
