/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('EquipmentTypeAddController', function() {

    beforeEach(function() {
        module('equipment-type-add');

        inject(function($injector) {
            this.$controller = $injector.get('$controller');
            this.$rootScope = $injector.get('$rootScope');
            this.confirmService = $injector.get('confirmService');
            this.$q = $injector.get('$q');
            this.stateTrackerService = $injector.get('stateTrackerService');
            this.$state = $injector.get('$state');
            this.equipmentTypeService = $injector.get('equipmentTypeService');
            this.loadingModalService = $injector.get('loadingModalService');
            this.notificationService = $injector.get('notificationService');
            this.EquipmentTypeDataBuilder = $injector.get('EquipmentTypeDataBuilder');

        });

        this.equipmentType = new this.EquipmentTypeDataBuilder().withoutId()
            .build();

        this.confirmDeferred = this.$q.defer();
        this.saveDeferred = this.$q.defer();
        var loadingDeferred = this.$q.defer();
        spyOn(this.confirmService, 'confirm').andReturn(this.confirmDeferred.promise);
        spyOn(this.stateTrackerService, 'goToPreviousState').andCallFake(loadingDeferred.resolve);
        spyOn(this.equipmentTypeService, 'save').andReturn(this.saveDeferred.promise);
        spyOn(this.$state, 'go');
        spyOn(this.loadingModalService, 'open').andReturn(loadingDeferred.promise);
        spyOn(this.loadingModalService, 'close').andCallFake(loadingDeferred.resolve);
        spyOn(this.notificationService, 'success');
        spyOn(this.notificationService, 'error');

        this.vm = this.$controller('EquipmentTypeAddController', {
            equipmentType: this.equipmentType
        });
        this.vm.$onInit();

        this.$rootScope.$apply();
    });

    describe('$onInit', function() {

        it('should expose this.stateTrackerService.goToPreviousState method', function() {

            expect(this.vm.goToPreviousState).toBe(this.stateTrackerService.goToPreviousState);
        });

    });

    describe('save', function() {

        it('should show notification if equipment category was saved successfully', function() {
            this.vm.save();

            this.confirmDeferred.reject();
            this.saveDeferred.resolve();
            this.$rootScope.$apply();

            expect(this.notificationService.success).toHaveBeenCalledWith(
                'equipmentType.typeHasBeenSaved'
            );
        });

        it('should show notification if equipment category save has failed', function() {
            this.vm.save();

            this.confirmDeferred.reject();
            this.saveDeferred.reject();
            this.$rootScope.$apply();

            expect(this.notificationService.error).toHaveBeenCalledWith(
                'equipmentType.failedToSaveType'
            );
        });

    });

});
