/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name equipment-inventory-item.inventoryItemService
     *
     * @description
     * Responsible for retrieving equipment inventory items from the server.
     */
    angular
        .module('equipment-inventory-item')
        .factory('inventoryItemService', service);

    service.$inject = ['equipmentUrlFactory', '$resource'];

    function service(equipmentUrlFactory, $resource) {

        var resource = $resource(equipmentUrlFactory('/api/equipmentInventoryItems/:id'), {}, {
            get: {
                transformResponse: transformGetResponse
            },
            query: {
                method: 'GET',
                url: equipmentUrlFactory('/api/equipmentInventoryItems'),
                transformResponse: transformGetAllResponse
            },
            update: {
                method: 'PUT'
            }
        });

        return {
            get: get,
            query: query,
            save: save
        };

        /**
         * @ngdoc method
         * @methodOf equipment-inventory-item.inventoryItemService
         * @name get
         *
         * @description
         * Gets equipment inventory item by id.
         *
         * @param  {String}  id equipment inventory item UUID
         * @return {Promise}    equipment inventory item info
         */
        function get(id) {
            return resource.get({
                id: id
            }).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-inventory-item.inventoryItemService
         * @name query
         *
         * @description
         * Query equipment inventory items.
         *
         * @param  {Object} params query parameters
         * @return {Promise}       Page of all equipment inventory items
         */
        function query(params) {
            return resource.query(params).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-inventory-item.inventoryItemService
         * @name update
         *
         * @description
         * Saves the given inventory item. It will create a new one if the ID is not defined and
         * update the existing one otherwise.
         *
         * @param  {Object}     inventoryItem   the updated inventory item
         * @return {Promise}                    the promise resolving to the updated item
         */
        function save(inventoryItem) {
            if (inventoryItem.id) {
                return resource.update({
                    id: inventoryItem.id
                }, inventoryItem).$promise;
            }
            return resource.save({}, inventoryItem).$promise;
        }

        function transformGetResponse(data, headers, status) {
            return transformResponse(data, status, transformInventoryItem);
        }

        function transformGetAllResponse(data, headers, status) {
            return transformResponse(data, status, function(response) {
                angular.forEach(response.content, function(inventoryItem) {
                    transformInventoryItem(inventoryItem);
                });
                return response;
            });
        }

        function transformInventoryItem(inventoryItem) {
            inventoryItem.decommissionDate = new Date(inventoryItem.decommissionDate);
            return inventoryItem;
        }

        function transformResponse(data, status, transformer) {
            if (status === 200) {
                return transformer(angular.fromJson(data));
            }
            return data;
        }
    }
})();
