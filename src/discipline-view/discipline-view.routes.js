/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular.module('discipline-view').config(routes);

    routes.$inject = ['$stateProvider', 'CCE_RIGHTS'];

    function routes($stateProvider, CCE_RIGHTS) {

        $stateProvider.state('openlmis.administration.discipline.edit', {
            label: 'disciplineView.edit',
            url: '/:id',
            accessRights: [CCE_RIGHTS.CCE_MANAGE],
            views: {
                '@openlmis': {
                    controller: 'DisciplineViewController',
                    templateUrl: 'discipline-view/discipline-view.html',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                discipline: function(DisciplineRepository, $stateParams) {
                    return new DisciplineRepository().get($stateParams.id);
                }
            }
        });
    }
})();
