/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('DisciplineViewController', function() {

    var vm, $q, $rootScope, $controller, $state, notificationService, loadingModalService,
        DisciplineRepository, DisciplineDataBuilder, discipline, $scope;

    beforeEach(function() {
        module('discipline-view');

        inject(function($injector) {
            $q = $injector.get('$q');
            $rootScope = $injector.get('$rootScope');
            $controller = $injector.get('$controller');
            $state = $injector.get('$state');
            notificationService = $injector.get('notificationService');
            loadingModalService = $injector.get('loadingModalService');
            DisciplineRepository = $injector.get('DisciplineRepository');
            DisciplineDataBuilder = $injector.get('DisciplineDataBuilder');
        });

        spyOn(DisciplineRepository.prototype, 'update').andReturn($q.when());

        var loadingModalPromise = $q.defer();
        spyOn(loadingModalService, 'close').andCallFake(loadingModalPromise.resolve);
        spyOn(loadingModalService, 'open').andReturn(loadingModalPromise.promise);

        spyOn(notificationService, 'success').andReturn(true);
        spyOn(notificationService, 'error').andReturn(true);
        spyOn($state, 'go').andCallFake(loadingModalPromise.resolve);

        discipline = new DisciplineDataBuilder().build();
        $scope =  $rootScope.$new();

        vm = $controller('DisciplineViewController', {
            $scope: $scope,
            discipline: discipline
        });
        vm.$onInit();
    });

    describe('$onInit', function() {
        it('should expose goToDisciplineList method', function() {

            expect(angular.isFunction(vm.goToDisciplineList)).toBe(true);
        });

        it('should expose saveDisciplineDetails method', function() {
            expect(angular.isFunction(vm.saveDisciplineDetails)).toBe(true);
        });

        it('should expose discipline', function() {
            expect(vm.discipline).toEqual(discipline);
        });

    });

    describe('goToDisciplineList', function() {

        it('should call state go with correct params', function() {
            vm.goToDisciplineList();

            expect($state.go).toHaveBeenCalledWith('openlmis.administration.discipline', {}, {
                reload: true
            });
        });
    });

    describe('saveDiscipline', function() {

        it('should open loading modal', function() {
            vm.saveDisciplineDetails();
            $rootScope.$apply();

            expect(loadingModalService.open).toHaveBeenCalled();
        });
    });

    it('should close loading modal and show error notification after save fails', function() {
        DisciplineRepository.prototype.update.andReturn($q.reject());
        vm.saveDisciplineDetails();
        $rootScope.$apply();

        expect(loadingModalService.close).toHaveBeenCalled();
        expect(notificationService.error).toHaveBeenCalledWith('disciplineView.saveDiscipline.fail');
    });

    it('should go to discipline list after successful save', function() {
        vm.saveDisciplineDetails();
        $rootScope.$apply();

        expect($state.go).toHaveBeenCalledWith('openlmis.administration.discipline', {}, {
            reload: true
        });
    });

    it('should show success notification after successful save', function() {
        vm.saveDisciplineDetails();
        $rootScope.$apply();

        expect(notificationService.success).toHaveBeenCalledWith('disciplineView.saveDiscipline.success');
    });

});
