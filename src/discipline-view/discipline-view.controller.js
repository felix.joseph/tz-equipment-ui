/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular
        .module('discipline-view')
        .controller('DisciplineViewController', controller);

    controller.$inject = [
        '$state', '$stateParams', 'CCE_RIGHTS', 'messageService', 'discipline', 'DisciplineRepository',
        'loadingModalService', '$q', 'notificationService'
    ];

    function controller($state, $stateParams,
                        CCE_RIGHTS, messageService, discipline, DisciplineRepository, loadingModalService,
                        $q, notificationService) {

        var vm = this;
        vm.$onInit = onInit;
        vm.saveDisciplineDetails = saveDisciplineDetails;
        vm.discipline = undefined;
        vm.goToDisciplineList = goToDisciplineList;
        function onInit() {
            vm.discipline = angular.copy(discipline);
            vm.discipline.enabled = discipline.enabled !== false;
        }

        function saveDisciplineDetails() {
            doSave(vm.discipline,
                'disciplineView.saveDiscipline.success',
                'disciplineView.saveDiscipline.fail');
        }

        function doSave(discipline, successMessage, errorMessage) {
            loadingModalService.open();
            return new DisciplineRepository().update(discipline)
                .then(function(discipline) {
                    notificationService.success(successMessage);
                    goToDisciplineList();
                    return $q.resolve(discipline);
                })
                .catch(function() {
                    notificationService.error(errorMessage);
                    loadingModalService.close();
                    return $q.reject();
                });
        }

        function goToDisciplineList() {
            $state.go('openlmis.administration.discipline', {}, {
                reload: true
            });
        }
    }
})();
