/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular
        .module('admin-operation-mode-edit')
        .controller('OperationModeEditController', controller);

    controller.$inject = [
        'CCE_RIGHTS', 'operationMode', 'operationModeService',
        'loadingModalService', '$q', 'notificationService', 'stateTrackerService'
    ];

    function controller(CCE_RIGHTS, operationMode, operationModeService, loadingModalService,
                        $q, notificationService, stateTrackerService) {

        var vm = this;
        vm.$onInit = onInit;
        vm.saveOperationModeDetails = saveOperationModeDetails;
        vm.operationMode = undefined;
        vm.goToPreviousState = stateTrackerService.goToPreviousState;
        function onInit() {
            vm.operationMode = operationMode;
        }

        function saveOperationModeDetails() {
            loadingModalService.open();
            return new operationModeService.save(vm.operationMode)
                .then(function(operationMode) {
                    notificationService.success('adminOperationModeEdit.edit.success');
                    stateTrackerService.goToPreviousState();
                    return $q.resolve(operationMode);
                })
                .catch(function() {
                    notificationService.error('adminOperationModeEdit.edit.fail');
                    loadingModalService.close();
                    return $q.reject();
                });
        }
    }
})();
