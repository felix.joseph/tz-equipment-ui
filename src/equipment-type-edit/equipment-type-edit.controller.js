/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipment-type.controller:EquipmentTypeEditController
     *
     * @description
     * Controller for editing equipment type.
     */
    angular
        .module('equipment-type-edit')
        .controller('EquipmentTypeEditController', EquipmentTypeEditController);

    EquipmentTypeEditController.$inject = [
        'equipmentType', '$state',  'stateTrackerService', 'loadingModalService',
        'equipmentTypeService', 'notificationService'
    ];

    function EquipmentTypeEditController(equipmentType, $state, stateTrackerService,
                                         loadingModalService, equipmentTypeService, notificationService) {

        var vm = this;

        vm.$onInit = onInit;
        vm.save = save;

        /**
         * @ngdoc property
         * @methodOf equipment-type.controller:EquipmentTypeEditController
         * @name equipmentType
         * @type {Object}
         *
         * @description
         * Current Equipment Type.
         */
        vm.equipmentType = equipmentType;

        vm.goToPreviousState = stateTrackerService.goToPreviousState;

        /**
         * @ngdoc method
         * @methodOf equipment-type.controller:EquipmentTypeEditController
         * @name $onInit
         *
         * @description
         * Method that is executed on initiating EquipmentTypeEditController.
         */
        function onInit() {
            vm.equipmentType = equipmentType;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-type.controller:EquipmentTypeEditController
         * @name save
         *
         * @description
         * Saves the Equipment Type.
         */
        function save() {
            loadingModalService.open();
            return new equipmentTypeService.save(vm.equipmentType)
                .then(function(equipmentType) {
                    notificationService.success('equipmentTypeEdit.typeHasBeenUpdated');
                    stateTrackerService.goToPreviousState();
                    return equipmentType;
                })
                .catch(function() {
                    notificationService.error('equipmentTypeEdit.failedToUpdateType');
                    loadingModalService.close();
                });
        }

    }

})();
