/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('EquipmentCategoryEditController', function() {

    beforeEach(function() {
        module('equipment-category-edit');

        inject(function($injector) {
            this.$controller = $injector.get('$controller');
            this.$rootScope = $injector.get('$rootScope');
            this.$q = $injector.get('$q');
            this.equipmentCategoryService = $injector.get('equipmentCategoryService');
            this.stateTrackerService = $injector.get('stateTrackerService');
            this.loadingModalService = $injector.get('loadingModalService');
            this.notificationService = $injector.get('notificationService');
            this.EquipmentCategoryDataBuilder = $injector.get('EquipmentCategoryDataBuilder');
            this.EquipmentTypeDataBuilder = $injector.get('EquipmentTypeDataBuilder');
            this.DisciplineDataBuilder = $injector.get('DisciplineDataBuilder');
        });

        this.equipmentCategory = new this.EquipmentCategoryDataBuilder().build();

        this.saveDeferred = this.$q.defer();

        spyOn(this.stateTrackerService, 'goToPreviousState').andReturn(true);
        spyOn(this.equipmentCategoryService, 'save').andReturn(this.saveDeferred.promise);
        spyOn(this.loadingModalService, 'open').andReturn(true);
        spyOn(this.loadingModalService, 'close').andReturn(true);
        spyOn(this.notificationService, 'success');
        spyOn(this.notificationService, 'error');

        this.equipmentTypes = [
            new this.EquipmentTypeDataBuilder().build(),
            new this.EquipmentTypeDataBuilder().build(),
            new this.EquipmentTypeDataBuilder().build()
        ];

        this.disciplines = [
            new this.DisciplineDataBuilder().build(),
            new this.DisciplineDataBuilder().build(),
            new this.DisciplineDataBuilder().build()
        ];

        this.vm = this.$controller('EquipmentCategoryEditController', {
            equipmentCategory: this.equipmentCategory,
            equipmentTypes: this.equipmentTypes,
            disciplines: this.disciplines
        });
        this.vm.$onInit();
    });

    describe('$onInit', function() {

        it('should expose equipmentCategory', function() {
            expect(this.vm.equipmentCategory).toEqual(this.equipmentCategory);
        });
    });

    describe('save', function() {

        it('should reload state after successful save', function() {
            this.vm.save();

            this.saveDeferred.resolve(this.equipmentCategory);
            this.$rootScope.$apply();

            expect(this.stateTrackerService.goToPreviousState).toHaveBeenCalled();
        });

        describe('while editing', function() {

            it('should show notification if equipment category was saved successfully', function() {
                this.vm.save();

                this.saveDeferred.resolve(this.equipmentCategory);
                this.$rootScope.$apply();

                expect(this.notificationService.success).toHaveBeenCalledWith('equipment.categoryHasBeenUpdated');
            });

            it('should show notification if equipment  type save has failed', function() {
                this.vm.save();

                this.saveDeferred.reject();
                this.$rootScope.$apply();

                expect(this.notificationService.error).toHaveBeenCalledWith('equipment.failedToUpdateCategory');
                expect(this.loadingModalService.close).toHaveBeenCalled();
            });
        });
    });
});