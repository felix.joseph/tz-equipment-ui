/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular
        .module('equipment-category-edit')
        .config(routes);

    routes.$inject = ['modalStateProvider', 'CCE_RIGHTS'];

    function routes(modalStateProvider) {

        modalStateProvider.state('openlmis.administration.equipmentCategory.edit', {
            controller: 'EquipmentCategoryEditController',
            templateUrl: 'equipment-category-edit/equipment-category-edit.html',
            controllerAs: 'vm',
            url: '/:id',
            resolve: {
                equipmentCategory: function($stateParams, equipmentCategoryService) {
                    return equipmentCategoryService.get($stateParams.id)
                        .then(function(response) {
                            return response;
                        });
                },
                equipmentTypes: equipmentTypesResolve,
                disciplines: disciplineResolve
            }
        });

    }

    equipmentTypesResolve.$inject = ['equipmentTypeService'];
    function equipmentTypesResolve(equipmentTypeService) {
        return equipmentTypeService.query({
            active: true
        })
            .then(function(response) {
                return response.content;
            });
    }

    disciplineResolve.$inject = ['disciplineService'];
    function disciplineResolve(disciplineService) {
        return disciplineService.getAll();
    }

})();
